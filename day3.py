from __future__ import division
import math
from math import sqrt
from array import array
import time

start_time = time.time()

inputfile = open("input3.txt", "r")
lines = inputfile.readlines()

#read the line
#go to spot i*3 on line i
#check if . or #

def part1(k):
    n = 0
    for i,a in enumerate(lines):
        element=k*i
        print len(a)
        while(element>30):
            element=element-31
        if str(a[element]) == "#":
            print "Ouch!"
            n+=1
        print i, element, a[element] 
    return n

def part2(k):
    n = 0
    count = 0
    for i,a in enumerate(lines):
        if (i % 2) != 0:
            continue
        element=k*count
        print len(a)
        while(element>30):
            element=element-31
        if str(a[element]) == "#":
            print "Ouch!"
            n+=1
        count +=1
        print count, element, a[element] 
    return n

print "done"
print("--- %s seconds ---" % (time.time() - start_time))

n = 1
for x in 1, 3, 5, 7:
    n = n*part1(x)
m = n*part2(1)
print m
