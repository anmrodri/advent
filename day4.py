from __future__ import division
import math
from math import sqrt
from array import array
import time

start_time = time.time()

inputfile = open("input4.txt", "r")
contents = inputfile.readlines()

#break into lines
#create dictionary
#check info

def part1():
    n = 0
    k = 1
    t = 0
    passport_dict = {}
    entry_array = ["byr","iyr","eyr","hgt","hcl","ecl","pid"]
    for line in contents:
        if len(line.strip()) != 0:
            passport = line.strip().split(' ')
            for variables in passport:
                key = variables.split(":")[0]
                value = variables.split(":")[1]
                passport_dict[key] = value
        else:
            for i in entry_array:
                entry = passport_dict.get(i)
                if str(entry) == "None":
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
                else:
                    if i == "byr": #byr (Birth Year) - four digits; at least 1920 and at most 2002.
                        if int(entry)<1920 or int(entry)>2002:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
                    elif i == "iyr": #four digits; at least 2010 and at most 2020.
                        if int(entry)<2010 or int(entry)>2020:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
                    elif i == "eyr": #four digits; at least 2020 and at most 2030.
                        if int(entry)<2020 or int(entry)>2030:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
                    elif i == "hgt": #a number followed by either cm or in: If cm, the number must be at least 150 and at most 193. If in, the number must be at least 59 and at most 76.
                        if entry[-2:] == "cm" and 150 <= int(entry[:-2]) <= 193:
                            continue
                        elif entry[-2:]== "in" and 59 <= int(entry[:-2]) <= 76:
                            continue
                        else:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
                    elif i == "hcl": #a # followed by exactly six characters 0-9 or a-f.
                        if entry[0] != "#" or len(entry) != 7:
                            t=1
                        for u in entry[1:]:
                            if u not in ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"]:
                                t=1
                                break

                        if t > 0:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            t = 0
                            break

                    elif i == "ecl": #exactly one of: amb blu brn gry grn hzl oth.
                        if entry not in ["amb","blu","brn","gry","grn","hzl","oth"]:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
                    elif i == "pid": #a nine-digit number, including leading zeroes.
                        if len(entry) == 9 and int(entry) >= 0:
                            continue
                        else:
                            n+=1
                            print "PASSPORT:", k, i, entry
                            break
            passport_dict = {}
            k += 1

    for i in entry_array:
        if str(passport_dict.get(i)) == "None":
            n+=1
            print "PASSPORT:", k, i, entry
            break
        else:
            if i == "byr": #byr (Birth Year) - four digits; at least 1920 and at most 2002.
                if int(entry)<1920 or int(entry)>2002:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
            elif i == "iyr": #four digits; at least 2010 and at most 2020.
                if int(entry)<2010 or int(entry)>2020:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
            elif i == "eyr": #four digits; at least 2020 and at most 2030.
                if int(entry)<2020 or int(entry)>2030:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
            elif i == "hgt": #a number followed by either cm or in: If cm, the number must be at least 150 and at most 193. If in, the number must be at least 59 and at most 76.
                if entry[-2:] == "cm" and 150 < int(entry[:-2]) < 193:
                    continue
                elif entry[-2:]== "in" and 59 < int(entry[:-2]) < 76:
                    continue
                else:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
            elif i == "hcl": #a # followed by exactly six characters 0-9 or a-f.
                if entry[0] != "#" or len(entry) != 7:
                    t=1
                for u in entry[1:]:
                    if u not in ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"]:
                        t=1
                        break

                if t > 0:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    t = 0
                    break
                    
            elif i == "ecl": #exactly one of: amb blu brn gry grn hzl oth.
                if entry not in ["amb","blu","brn","gry","grn","hzl","oth"]:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break
            elif i == "pid": #a nine-digit number, including leading zeroes.
                if len(entry) == 9 and int(entry)>=0:
                    continue
                else:
                    n+=1
                    print "PASSPORT:", k, i, entry
                    break

    passport_dict = {}
    print "Total:", k
    print "Number ok:", k-n

part1()
print "done"
print("--- %s seconds ---" % (time.time() - start_time))

