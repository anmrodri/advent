from __future__ import division
import math
from math import sqrt
from array import array
import time

start_time = time.time()

inputfile = open("inputday1.txt", "r")
lines = inputfile.readlines()
done=0
for a in lines:
    a=int(a)
    for b in lines:
        b=int(b)
        if a != b:
            f = a+b
            if f == 2020:
                g = a*b
                print "Part 1",g
            else:
                for c in lines:
                    if done == 1:
                        break
                    c=int(c)
                    if a != c:
                        e = f+c
                        if e == 2020:
                            d = a*b*c
                            print "Part 2",d 
                            done=1
print "done"
print("--- %s seconds ---" % (time.time() - start_time))
