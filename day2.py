from __future__ import division
import math
from math import sqrt
from array import array
import time

start_time = time.time()

inputfile = open("input2.txt", "r")
lines = inputfile.readlines()

def part1():
    n=0
    for k,a in enumerate(lines):
        rule_min = int(a.split(" ")[0].split("-")[0])
        rule_max = int(a.split(" ")[0].split("-")[1])
        rule_char = a.split(" ")[1][:-1]
        password = a.split(" ")[2]

        b = 0
        for i in password:
            if i == rule_char: b+= 1
        if rule_min <= b <= rule_max:
            n+=1
    print n

def part2():
    n=0
    for k,a in enumerate(lines):
        rule_min = int(a.split(" ")[0].split("-")[0])
        rule_max = int(a.split(" ")[0].split("-")[1])
        rule_char = a.split(" ")[1][:-1]
        password = a.split(" ")[2]

        b = 0
        for enum, i in enumerate(password):
            if (enum+1 == rule_min and i == rule_char): b+=1
            if (enum+1 == rule_max and i == rule_char): b+=1
            
        if b == 1:
            n+=1
    print n

part1()
print "Part 2"
part2()
print "done"
print("--- %s seconds ---" % (time.time() - start_time))
